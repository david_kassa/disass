from setuptools import setup
from setuptools import find_packages
from disass import version

requirements_txt_file_path = 'requirements.txt'

with open(requirements_txt_file_path, 'r') as requirements_txt_file:
    requirements_txt = requirements_txt_file.readlines()

setup(name='disass',
      version=version(),
      description='Reverse engineering pragmatically',
      url='https://gitlab.com/david_kassa/disass',
      author='David Kassa',
      packages=find_packages(),
      zip_safe=False,
      python_requires='>=3.8',
      classifiers=[
          'Development Status :: 1 - Planning',
          'Intended Audience :: Developers',
          'Intended Audience :: Science/Research',
          'Topic :: Software Development :: Disassemblers',
          'Programming Language :: Python :: 3',
          'License :: OSI Approved :: ISC License (ISCL)',
      ],
      install_requires=requirements_txt)
