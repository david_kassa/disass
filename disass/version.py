__version__ = (0, 1, 0)


def version():
    return '{}.{}.{}'.format(*__version__)
